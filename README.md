# Lab 1. Introduction to Local Search

## TODO:

Fill missing code according to the `TODO:` comments in the following files:
- `local_search/algorithms/hill_climbing/best_choice_hill_climbing.py`
- `local_search/algorithms/hill_climbing/random_choice_hill_climbing.py`
- `local_search/algorithms/hill_climbing/worst_choice_hill_climbing.py`
- `local_search/algorithms/hill_climbing/simulated_annealing.py`
- `local_search/problems/graph_coloring_problem/model.py`
- `local_search/problems/graph_coloring_problem/moves/kempe_chain.py`
- `local_search/problems/graph_coloring_problem/goals/goal.py`

## How To Run

```python
pip install -e .
python run.py solve -c tsp_config.json
```